import React from "react";

export const Footer = () => {
	return (
		<div className="bg-dark fixed-bottom mt-5">
			<p className="text-center text-white mb-0 py-1">
				<i>Creado por Ricardo Jaramillo CAR IV 2022</i>
			</p>
		</div>
	);
};
