import { useEffect, useState } from "react";

import { ListEpisode } from "./Episodios/ListEpisode";
import axios from "axios";
import { useParams } from "react-router-dom";

const getStyle = (status) => {
	let color = "#ACFF33";
	if (status == "Alive") {
		color = "#ACFF33";
	}
	if (status == "Dead") {
		color = "red";
	}
	if (status == "unknown") {
		color = "#DEDADA";
	}
	let style = {
		display: "inline-block",
		marginRight: "6px",
		width: "20px",
		height: "20px",
		borderRadius: "50%",
		backgroundColor: color,
	};

	return style;
};

export function Personaje() {
	let { personajeId } = useParams();
	const [personaje, setPersonaje] = useState(null);
	const [episodios, setEpisodios] = useState(null);

	useEffect(() => {
		const getPersonaje = async (url) => {
			let result = await axios.get(url);
			setPersonaje(result.data);
		};
		getPersonaje(`https://rickandmortyapi.com/api/character/${personajeId}`);
	}, [personajeId]);

	useEffect(() => {
		if (personaje) {
			let peticionesEp = personaje.episode.map((ep) => {
				return axios.get(ep);
			});

			Promise.all(peticionesEp).then((respuesta) => {
				setEpisodios(respuesta);
			});
		}
	}, [personaje]);
	return (
		<div className="container">
			{personaje ? (
				<>
					<h1 className="text-center py-1">{personaje.name}</h1>
					<div className="card mb-3">
						<img
							src={personaje.image}
							className=" img-fluid card-img-top"
							alt={personaje.name}
							style={{ objectFit: "fill" }}
						/>
						<div className="card-body">
							<h3 className="card-title">
								<span style={getStyle(personaje.status)}></span>
								{personaje.status} - {personaje.species}
							</h3>
							<div>
								{episodios ? (
									<ListEpisode episodios={episodios} />
								) : (
									<p>Cargando...</p>
								)}
							</div>
						</div>
					</div>
				</>
			) : (
				<p>Cargando...</p>
			)}
		</div>
	);
}
