export function Buscador({ getBuscador, textSearch }) {
	return (
		<div className="d-flex justify-content-end pb-3 pt-1 px-2">
			<div className="col-md-6 col-xxl-4">
				<input
					type="text"
					className="form-control shadow"
					placeholder="Buscador"
					onChange={getBuscador}
					value={textSearch}
				/>
			</div>
		</div>
	);
}
