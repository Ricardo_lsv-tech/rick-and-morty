import React from "react";

export const EpisodeItem = ({ name, air_date, episode }) => {
	return (
		<div className=" col-lg-4 col-sm-6 col-xxl-3 mb-2">
			<div className="card shadow" style={{ width: "18rem" }}>
				<div className="card-header">
					<h5 className="card-title">{air_date}</h5>
				</div>
				<div className="card-body">
					<h6 className="card-subtitle mb-2 text-muted">{name}</h6>
					<p className="card-text">{episode}</p>
				</div>
			</div>
		</div>
	);
};
