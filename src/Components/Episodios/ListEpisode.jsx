import { EpisodeItem } from "./EpisodeItem";
import React from "react";

export const ListEpisode = ({ episodios }) => {
	return (
		<div>
			<h2 className="py-5">EPISODIOS</h2>
			<div className="row" style={{ marginBottom: "150px" }}>
				{episodios ? (
					episodios.map((episodio) => (
						<EpisodeItem key={episodio.data.id} {...episodio.data} />
					))
				) : (
					<p>Cargando...</p>
				)}
			</div>
		</div>
	);
};
