import { Link } from "react-router-dom";

export function Menu() {
	return (
		<nav className="navbar navbar-expand-lg navbar-light bg-dark mb-1 py-3">
			<div className="container-fluid">
				<Link className="navbar-brand text-white" to="/">
					<h2>RICK and MORTY</h2>
				</Link>
				<div className="collapse navbar-collapse" id="navbarText">
					<ul className="navbar-nav me-auto mb-2 mb-lg-0">
						<li className="nav-item">
							<Link
								className="nav-link active text-white"
								aria-current="page"
								to="/">
								<h4>Inicio</h4>
							</Link>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	);
}
