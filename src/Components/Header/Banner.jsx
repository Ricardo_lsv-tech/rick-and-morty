import BannerImg from "../../../assets/images/rickandmorty.jpg";

export function Banner() {
	return (
		<div>
			<div className="carousel-inner">
				<div className="carousel-item active">
					<img src={BannerImg} className="d-block w-100" alt="Banner" />
				</div>
			</div>
		</div>
	);
}
