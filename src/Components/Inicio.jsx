import { useEffect, useState } from "react";

import { Banner } from "./Header/Banner";
import { Buscador } from "./Buscador/Buscador";
import { ListadoPersonajes } from "./Personajes/ListadoPersonajes";
import React from "react";
import axios from "axios";

export function Inicio() {
	const [data, setData] = useState([]);
	const [page, setPage] = useState(1);
	const [textSearch, setTextSearch] = useState("");
	const [filtro, setFiltro] = useState();

	const getPage = (pageNumber) => {
		console.log(pageNumber);
		setPage(pageNumber);
	};

	const getBuscador = (e) => {
		setTextSearch(e.target.value);
		let buscar = data.filter((element) => {
			if (element.name.toLowerCase().includes(e.target.value.toLowerCase())) {
				return element;
			}
		});
		setFiltro(buscar);
	};

	useEffect(() => {
		const getData = async (url) => {
			let result = await axios.get(url);
			setData(result.data.results);
		};
		getData(`https://rickandmortyapi.com/api/character?page=${page}`);
	}, [page]);

	return (
		<div>
			<Banner />
			<h1 className="pt-4 pb-2 text-center">PERSONAJES DESTACADOS</h1>
			<Buscador getBuscador={getBuscador} textSearch={textSearch} />
			<ListadoPersonajes
				data={filtro ? filtro : data}
				getPage={getPage}
				pagina={page}
			/>
		</div>
	);
}
