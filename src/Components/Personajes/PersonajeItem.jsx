import { Link } from "react-router-dom";

const getStyle = (status) => {
	let color = "#ACFF33";
	if (status == "Alive") {
		color = "#ACFF33";
	}
	if (status == "Dead") {
		color = "red";
	}
	if (status == "unknown") {
		color = "#DEDADA";
	}
	let style = {
		display: "inline-block",
		marginRight: "5px",
		width: "10px",
		height: "10px",
		borderRadius: "50%",
		backgroundColor: color,
	};

	return style;
};

export function PersonajeItem({ personaje }) {
	return (
		<Link
			to={`/personaje/${personaje.id}`}
			style={{ textDecoration: "none", color: "initial" }}
			className="col-lg-4 col-sm-6 col-xxl-3 col-xs-6">
			<div className="shadow card mb-3 mx-2" style={{ maxWidth: "540px" }}>
				<div className="row g-0">
					<div className="col-4">
						<img
							src={personaje.image}
							className="img-fluid rounded-start img-responsive"
							alt="image one"
							style={{ height: "100%", objectFit: "cover" }}
						/>
					</div>
					<div className="col-8">
						<div className="card-body">
							<h5 className="card-title">{personaje.name}</h5>
							<p className="card-text">
								<span style={getStyle(personaje.status)}></span>
								{personaje.status} - {personaje.species}
							</p>
							<p className="card-text text-muted mb-1">Last known location:</p>
							<p className="card-text">{personaje.location.name}</p>
							<p className="card-text text-muted mb-1">First see in:</p>
							<p className="card-text">{personaje.origin.name}</p>
						</div>
					</div>
				</div>
			</div>
		</Link>
	);
}
