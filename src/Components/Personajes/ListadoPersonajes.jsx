import Pagination from "react-js-pagination";
import { PersonajeItem } from "./PersonajeItem";

export function ListadoPersonajes({ data, getPage, pagina }) {
	const handlePageChange = (page) => {
		getPage(page);
	};
	return (
		<>
			<div className="row">
				{data.map((personaje) => (
					<PersonajeItem key={personaje.id} personaje={personaje} />
				))}
			</div>
			<div
				className="d-flex justify-content-center"
				style={{ marginBottom: "200px" }}>
				<Pagination
					itemClass="page-item"
					linkClass="page-link"
					activePage={pagina}
					itemsCountPerPage={19.6}
					totalItemsCount={820}
					pageRangeDisplayed={5}
					onChange={handlePageChange}
				/>
			</div>
		</>
	);
}
