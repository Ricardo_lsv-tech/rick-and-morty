import "./App.css";

import { Route, BrowserRouter as Router, Routes } from "react-router-dom";

import { Footer } from "./Components/Footer/Footer";
import { Inicio } from "./Components/Inicio";
import { Menu } from "./Components/Header/Menu";
import { Personaje } from "./Components/Personaje";

function App() {
	return (
		<Router>
			<Menu />
			<Routes>
				<Route path="/Personaje/:personajeId" element={<Personaje />} />
				<Route path="/" element={<Inicio />} />
			</Routes>
			<Footer />
		</Router>
	);
}

export default App;
